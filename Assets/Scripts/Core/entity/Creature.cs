﻿using System.Collections.Generic;
using Core.entity.ai;
using UnityEngine;
using UnityEngine.AI;

namespace Core.entity
{
    public class Creature : LivingEntity, IEntityAI
    {
        private NavMeshAgent _ai;
        private Queue<Vector3> _path = new Queue<Vector3>();

        protected override void Awake()
        {
            base.Awake();
            _ai = GetComponent<NavMeshAgent>();
        }

        public void SetDestination(IEnumerable<Vector3> targetPosition)
        {
            _path = new Queue<Vector3>(targetPosition);
        }

        public void SetDestination(Vector3 targetPosition)
        {
            _path = new Queue<Vector3>();
            _path.Enqueue(targetPosition);
        }


        public void NextDestination()
        {
            _ai.SetDestination(_path.Dequeue());
        }

        private void CheckPath()
        {
            if (_path.Count < 1) return;
            if (_ai.remainingDistance > 1f) return;
            NextDestination();
        }

        private void FixedUpdate()
        {
            CheckPath();
        }
    }
}