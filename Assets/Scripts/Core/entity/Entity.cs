﻿using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

namespace Core.entity
{
    public class Entity : MonoBehaviour, IDamageable, IInteractable
    {
        [SerializeField] private float maxHealth;
        public bool IsDead { get; private set; }
        private float _health;
        public event UnityAction<Entity, float> EntityDamageEvent;
        public event UnityAction<Entity> EntityDeathEvent;

        protected virtual void Awake()
        {
            _health = maxHealth;
        }

        public Vector3 GetLocation() => transform.position;

        public void SetLocation(Vector3 location)
        {
            transform.position = location;
        }

        public void SetRotation(Quaternion rotation)
        {
            transform.rotation = rotation;
        }

        public float GetHealth() => _health;

        public void SetHealth(float amount)
        {
            _health = amount;
        }

        public float GetMaxHealth()
        {
            return maxHealth;
        }

        public void Damage(float amount)
        {
            if (amount <= 0f) return;

            var newHealth = GetHealth() - amount;
            SetHealth(newHealth);
            EntityDamageEvent?.Invoke(this, amount);

            if (newHealth > 0) return;
            IsDead = true;
            EntityDeathEvent?.Invoke(this);
            Destroy(gameObject);
        }

        public virtual void Interact(Player player)
        {
        }

        public Vector3 GetVelocity()
        {
            return TryGetComponent<NavMeshAgent>(out var agent) ? agent.velocity : Vector3.zero;
        }
    }
}