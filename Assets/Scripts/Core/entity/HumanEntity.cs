﻿using Core.inventory;

namespace Core.entity
{
    public class HumanEntity : LivingEntity, IInventoryHolder
    {
        private PlayerInventory _inventory;
        private IInventoryView _openInventory;

        protected override void Awake()
        {
            base.Awake();
            _inventory = new PlayerInventory(this, 2, InventoryType.Default);
        }

        Inventory IInventoryHolder.GetInventory()
        {
            return GetInventory();
        }

        public PlayerInventory GetInventory()
        {
            return _inventory;
        }

        public IInventoryView GetOpenInventory()
        {
            return _openInventory;
        }

        public IInventoryView OpenInventory(Inventory inventory)
        {
            _openInventory = InventoryInstance.OpenInventory(inventory);
            inventory.SetViewer(this);
            return _openInventory;
        }

        public void OpenInventory(IInventoryView inventoryView)
        {
            _openInventory = inventoryView;
            inventoryView.Open();
        }

        public void CloseInventory()
        {
            _openInventory.Close();
            _openInventory = null;
        }
    }
}