﻿namespace Core.entity
{
    public interface IDamageable
    {
        void Damage(float amount);
    }
}