﻿namespace Core.entity
{
    public interface IInteractable
    {
        void Interact(Player player);
    }
}