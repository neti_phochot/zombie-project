﻿using Core.entity.ai;

namespace Core.entity
{
    public abstract class LivingEntity : Entity
    {
        public IEntityTargetProvider TargetProvider { get; private set; }

        protected override void Awake()
        {
            base.Awake();
            TargetProvider = GetComponent<IEntityTargetProvider>();
        }

        /// <summary>
        /// Makes this entity attack the given entity with a melee attack. Attack damage is calculated by the server from the attributes and equipment of this mob, and knockback is applied to target as appropriate.
        /// </summary>
        /// <param name="target"></param>
        public void Attack(Entity target)
        {
            //TODO
            var damage = 1f;
            target.Damage(damage);
        }
    }
}