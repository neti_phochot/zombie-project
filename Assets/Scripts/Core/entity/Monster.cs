﻿using Core.loot;
using UnityEngine;

namespace Core.entity
{
    public class Monster : Creature, ILootAble
    {
        [SerializeField] private int moneyDropAmount;
        public int GetDropMoney() => moneyDropAmount;
    }
}