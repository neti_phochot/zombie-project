﻿using Core.inventory;
using UnityEngine;

namespace Core.entity
{
    public class Player : HumanEntity
    {
        [SerializeField] private ItemStack testItem;

        private void Start()
        {
            for (var i = 0; i < 2; i++)
            {
                var gun = Instantiate(testItem);
                GetInventory().AddItem(gun);
            }

            GetInventory().SetItemInMainHand(GetInventory().GetItemInMainHand());
        }

        private bool IsInteractAble()
        {
            //TODO: Statistic
            return GetOpenInventory() == null;
        }

        public void UseItem(ItemStack itemStack, ActionType actionType)
        {
            if (!IsInteractAble()) return;
            itemStack.UseItem(this, actionType);
        }

        public void TryInteract()
        {
            if (!IsInteractAble()) return;
            var lookingEntity = TargetProvider.GetLookingEntity();
            if (!lookingEntity) return;
            lookingEntity.Interact(this);
        }
    }
}