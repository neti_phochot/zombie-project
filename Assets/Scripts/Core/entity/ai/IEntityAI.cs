﻿using System.Collections.Generic;
using UnityEngine;

namespace Core.entity.ai
{
    public interface IEntityAI
    {
        void SetDestination(IEnumerable<Vector3> targetPosition);
        void SetDestination(Vector3 targetPosition);
        void NextDestination();
    }
}