﻿using UnityEngine;

namespace Core.entity.ai
{
    public interface IEntityTargetProvider
    {
        Entity GetLookingEntity();
        Vector3 GetLookingLocation();
        Transform GetEyesTransform();
        Vector3 GetDirection();
    }
}