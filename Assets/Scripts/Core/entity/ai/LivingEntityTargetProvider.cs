﻿using UnityEngine;

namespace Core.entity.ai
{
    public class LivingEntityTargetProvider : MonoBehaviour, IEntityTargetProvider
    {
        [SerializeField] private Transform eyesTransform;
        [SerializeField] private float trackTargetDistance = 100f;
        [SerializeField] private LayerMask interactLayerMask;
        [SerializeField] private LayerMask trackLocationLayerMask;

        private Entity _lookingEntity;
        private Vector3 _lookingLocation;

        private void FixedUpdate()
        {
            UpdateLookingEntity();
            UpdateLookingLocation();
        }

        private void UpdateLookingLocation()
        {
            if (!Physics.Raycast(eyesTransform.position, eyesTransform.forward,
                out var hit, trackTargetDistance, trackLocationLayerMask)) return;
            _lookingLocation = hit.point;
        }

        private void UpdateLookingEntity()
        {
            if (!Physics.Raycast(eyesTransform.position, eyesTransform.forward,
                out var hit, trackTargetDistance, interactLayerMask))
            {
                _lookingEntity = null;
                return;
            }

            if (!hit.transform.TryGetComponent<Entity>(out var entity))
            {
                _lookingEntity = null;
                return;
            }

            _lookingEntity = entity;
        }

        public Entity GetLookingEntity() => _lookingEntity;
        public Vector3 GetLookingLocation() => _lookingLocation;
        public Transform GetEyesTransform() => eyesTransform;
        public Vector3 GetDirection() => eyesTransform.forward;
    }
}