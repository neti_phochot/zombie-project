﻿using UnityEngine;

namespace Core.gameevent
{
    public abstract class BaseRegisterEvent : MonoBehaviour
    {
        public virtual void Start()
        {
            RegisterEvents();
        }

        protected abstract void RegisterEvents();
    }
}