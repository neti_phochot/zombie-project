﻿using Singleton;
using UnityEngine;

namespace Core.gameevent
{
    public class EventInstance : ResourceSingleton<EventInstance>
    {
        [SerializeField] private EventSO eventSo;
        public static EventSO Event => Instance.eventSo;
    }
}