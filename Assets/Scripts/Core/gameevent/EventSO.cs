﻿using Core.entity;
using Core.inventory;
using Core.projectile;
using UnityEngine;
using UnityEngine.Events;

namespace Core.gameevent
{
    [CreateAssetMenu(menuName = "Event")]
    public class EventSO : ScriptableObject
    {
        //ENTITY
        public event UnityAction<Entity, float> EntityDamageEvent;
        public event UnityAction<Entity> EntityDeathEvent;

        public void OnEntityDamageEvent(Entity entity, float damage)
        {
            EntityDamageEvent?.Invoke(entity, damage);
        }

        public void OnEntityDeathEvent(Entity entity)
        {
            EntityDeathEvent?.Invoke(entity);
        }

        //PROJECTILE
        public event UnityAction<Projectile, Entity> ProjectileHitEntityEvent;
        public event UnityAction<Projectile> ProjectileHitEvent;

        public void OnProjectileHitEntityEvent(Projectile projectile, Entity entity)
        {
            ProjectileHitEntityEvent?.Invoke(projectile, entity);
        }

        public void OnProjectileHitEvent(Projectile projectile)
        {
            ProjectileHitEvent?.Invoke(projectile);
        }

        //INVENTORY
        public event UnityAction<IInventoryView> InventoryOpenEvent;
        public event UnityAction<IInventoryView> InventoryCloseEvent;
        public event UnityAction<IInventoryView> InventoryClickEvent;
        public event UnityAction<ItemStack> InventoryPickUpItemEvent;

        public void OnInventoryOpenEvent(IInventoryView inventoryView)
        {
            InventoryOpenEvent?.Invoke(inventoryView);
        }

        public void OnInventoryCloseEvent(IInventoryView inventoryView)
        {
            InventoryCloseEvent?.Invoke(inventoryView);
        }

        public void OnInventoryClickEvent(IInventoryView inventoryView)
        {
            InventoryClickEvent?.Invoke(inventoryView);
        }

        public void OnInventoryPickUpItemEvent(ItemStack itemStack)
        {
            InventoryPickUpItemEvent?.Invoke(itemStack);
        }
    }
}