﻿using Core.entity;
using UnityEngine;

namespace Core.gameevent.entity
{
    [RequireComponent(typeof(Entity))]
    public class EntityEventRegisterEvent : BaseRegisterEvent
    {
        protected override void RegisterEvents()
        {
            var entity = GetComponent<Entity>();
            entity.EntityDamageEvent += OnEntityDamageEvent;
            entity.EntityDeathEvent += OnEntityDeathEvent;
        }

        private void OnEntityDamageEvent(Entity entity, float damage)
        {
            EventInstance.Event.OnEntityDamageEvent(entity, damage);
        }

        private void OnEntityDeathEvent(Entity entity)
        {
            EventInstance.Event.OnEntityDeathEvent(entity);
        }
    }
}