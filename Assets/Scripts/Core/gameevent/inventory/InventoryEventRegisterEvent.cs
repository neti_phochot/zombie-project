﻿using Core.entity;
using Core.inventory;
using UnityEngine;

namespace Core.gameevent.inventory
{
    [RequireComponent(typeof(InventoryView))]
    public class InventoryEventRegisterEvent : BaseRegisterEvent
    {
        protected override void RegisterEvents()
        {
            var inventoryView = GetComponent<IInventoryView>();
            inventoryView.InventoryOpenEvent += InventoryOpenEvent;
            inventoryView.InventoryCloseEvent += OnInventoryCloseEvent;
            inventoryView.InventoryClickEvent += OnInventoryClickEvent;
        }

        private void InventoryOpenEvent(IInventoryView inventoryView)
        {
            EventInstance.Event.OnInventoryOpenEvent(inventoryView);
        }

        private void OnInventoryCloseEvent(IInventoryView inventoryView)
        {
            EventInstance.Event.OnInventoryCloseEvent(inventoryView);
        }

        private void OnInventoryClickEvent(IInventoryView inventoryView)
        {
            EventInstance.Event.OnInventoryClickEvent(inventoryView);
        }
    }
}