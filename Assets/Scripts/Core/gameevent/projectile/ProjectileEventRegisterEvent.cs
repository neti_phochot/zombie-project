﻿using Core.entity;
using Core.projectile;
using UnityEngine;

namespace Core.gameevent.projectile
{
    [RequireComponent(typeof(Projectile))]
    public class ProjectileEventRegisterEvent : BaseRegisterEvent
    {
        protected override void RegisterEvents()
        {
            var projectile = GetComponent<Projectile>();
            projectile.ProjectileHitEntityEvent += ProjectileHitEntityEvent;
            projectile.ProjectileHitEvent += OnProjectileHitEvent;
        }

        private void ProjectileHitEntityEvent(Projectile projectile, Entity entity)
        {
            EventInstance.Event.OnProjectileHitEntityEvent(projectile, entity);
        }

        private void OnProjectileHitEvent(Projectile projectile)
        {
            EventInstance.Event.OnProjectileHitEvent(projectile);
        }
    }
}