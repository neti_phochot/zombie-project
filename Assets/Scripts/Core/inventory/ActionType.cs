﻿namespace Core.inventory
{
    public enum ActionType
    {
        UseItemDown,
        UseItemUp,
        ConsumeItemDown,
        ConsumeItemUp,
    }
}