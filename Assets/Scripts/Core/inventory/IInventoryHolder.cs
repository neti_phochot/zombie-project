﻿namespace Core.inventory
{
    public interface IInventoryHolder
    {
        Inventory GetInventory();
    }
}