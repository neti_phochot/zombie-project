﻿using Core.entity;
using UnityEngine.Events;

namespace Core.inventory
{
    public interface IInventoryView
    {
        event UnityAction<IInventoryView> InventoryClickEvent;
        event UnityAction<IInventoryView> InventoryOpenEvent;
        event UnityAction<IInventoryView> InventoryCloseEvent;
        void Init(Inventory inventory);
        void Open();
        void Close();
        ItemStack GetCursor();
        HumanEntity GetPlayer();
    }
}