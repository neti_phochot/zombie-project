﻿using System.Collections.Generic;
using Core.entity;

namespace Core.inventory
{
    public class Inventory
    {
        private readonly InventoryType _inventoryType;
        private ItemStack[] _itemStacks;
        private readonly IInventoryHolder _inventoryHolder;
        private HumanEntity _viewer;

        public Inventory(IInventoryHolder inventoryHolder, int inventorySize, InventoryType inventoryType)
        {
            _itemStacks = new ItemStack[inventorySize];
            _inventoryHolder = inventoryHolder;
            _inventoryType = inventoryType;
        }

        public void SetViewer(HumanEntity viwer)
        {
            _viewer = viwer;
        }

        public HumanEntity GetViewer()
        {
            return _viewer;
        }

        public int FirstEmpty()
        {
            for (var i = 0; i < _itemStacks.Length; i++)
                if (_itemStacks[i] == null)
                    return i;

            return -1;
        }

        public void AddItem(ItemStack itemStack)
        {
            _itemStacks[FirstEmpty()] = itemStack;
        }

        public void SetStorageContents(ItemStack[] items)
        {
            _itemStacks = items;
        }

        public void SetItem(int index, ItemStack itemStack)
        {
            _itemStacks[index] = itemStack;
        }

        public ItemStack GetItem(int index)
        {
            return _itemStacks[index];
        }

        public IEnumerable<ItemStack> GetContents()
        {
            return _itemStacks;
        }

        public InventoryType GetInventoryType()
        {
            return _inventoryType;
        }

        public int GetSize()
        {
            return _itemStacks.Length;
        }

        public IInventoryHolder GetHolder()
        {
            return _inventoryHolder;
        }
    }
}