﻿using Core.gameevent;
using TowerDefense.gameevent;
using Utils;

namespace Core.inventory
{
    public class InventoryController : BaseListener
    {
        protected override void RegisterEvents()
        {
            EventInstance.Event.InventoryOpenEvent += OnInventoryOpenEvent;
            EventInstance.Event.InventoryCloseEvent += OnInventoryCloseEvent;
        }

        private void OnInventoryOpenEvent(IInventoryView inventoryView)
        {
            CursorManager.LockCursor();
            CursorManager.UnlockCursor();
        }

        private void OnInventoryCloseEvent(IInventoryView inventoryView)
        {
            CursorManager.UnlockCursor();
            CursorManager.LockCursor();
        }
    }
}