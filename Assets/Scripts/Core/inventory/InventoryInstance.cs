﻿using System.Collections.Generic;
using Singleton;
using UnityEngine;

namespace Core.inventory
{
    public class InventoryInstance : MonoSingleton<InventoryInstance>
    {
        [SerializeField] private InventoryView defaultInventory;

        private Dictionary<InventoryType, IInventoryView> _inventoriesPrefab;

        public override void Awake()
        {
            base.Awake();
            Debug.Assert(defaultInventory != null, "defaultInventory can't be null!");
            InitInventory();
        }

        private void InitInventory()
        {
            _inventoriesPrefab = new Dictionary<InventoryType, IInventoryView>()
            {
                {InventoryType.Default, Instantiate(defaultInventory)}
            };
        }

        public static IInventoryView OpenInventory(Inventory inventory)
        {
            var inventoryView = Instance._inventoriesPrefab[inventory.GetInventoryType()];
            inventoryView.Init(inventory);
            inventoryView.Open();
            return inventoryView;
        }
    }
}