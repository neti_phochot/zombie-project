﻿using System.Collections.Generic;
using Core.entity;
using Core.ui.inventory;
using UnityEngine;
using UnityEngine.Events;

namespace Core.inventory
{
    public class InventoryView : MonoBehaviour, IInventoryView
    {
        [SerializeField] private Transform container;
        [SerializeField] private ItemSlot itemSlotTemplate;

        private ItemStack _cursorItem;
        private Inventory _inventory;
        public event UnityAction<IInventoryView> InventoryClickEvent;
        public event UnityAction<IInventoryView> InventoryOpenEvent;
        public event UnityAction<IInventoryView> InventoryCloseEvent;

        private void Awake()
        {
            Debug.Assert(container != null, "container can't be null!");
            Debug.Assert(itemSlotTemplate != null, "itemSlotTemplate can't be null!");
            Close();
        }

        public void Init(Inventory inventory)
        {
            _inventory = inventory;
            Clear();
            AddAllItem(inventory.GetContents()); //TODO: Clone Items 
        }

        private void Clear()
        {
            foreach (Transform item in container)
                Destroy(item.gameObject);
        }

        private void AddAllItem(IEnumerable<ItemStack> itemStacks)
        {
            itemSlotTemplate.gameObject.SetActive(true);
            foreach (var item in itemStacks)
            {
                if (!item) continue;
                var inventoryItem = Instantiate(itemSlotTemplate, container);
                inventoryItem.Init(item);
                inventoryItem.OnClick += OnItemStackClick;
            }

            itemSlotTemplate.gameObject.SetActive(false);
        }

        private void OnItemStackClick(ItemStack itemStack)
        {
            _cursorItem = itemStack;
            itemStack.ClickItem(this);
            InventoryClickEvent?.Invoke(this);
        }

        public void Open()
        {
            gameObject.SetActive(true);
            InventoryOpenEvent?.Invoke(this);
        }

        public void Close()
        {
            gameObject.SetActive(false);
            InventoryCloseEvent?.Invoke(this);
        }

        public ItemStack GetCursor()
        {
            return _cursorItem;
        }

        public HumanEntity GetPlayer()
        {
            return _inventory.GetViewer();
        }
    }
}