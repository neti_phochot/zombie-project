﻿using System;
using Core.entity;
using Core.inventory.meta;
using UnityEngine;

namespace Core.inventory
{
    public class ItemStack : MonoBehaviour, IItemMeta, meta.IDamageable, IComparable<ItemStack>
    {
        [Header("ITEM UI")] [SerializeField] private Sprite itemIcon;
        [Tooltip("Optional")] [SerializeField] private Sprite itemHoverIcon;

        [Header("ITEM INFORMATION")] [SerializeField]
        private string itemName;

        [SerializeField] private string itemLore;

        [SerializeField] private float itemDamage;

        public virtual void Awake()
        {
            Debug.Assert(itemIcon != null, "itemIcon can't be null.");
        }

        public Sprite ItemIcon => itemIcon;
        public Sprite ItemHoverIcon => itemHoverIcon;
        public string GetDisplayName() => itemName;

        public void SetDisplayName(string itemStackName)
        {
            itemName = itemStackName;
        }

        public string GetLore() => itemLore;

        public void SetLore(string lore)
        {
            itemLore = lore;
        }

        public float GetDamage() => itemDamage;
        public bool HasDamage() => itemDamage > 0f;

        public void SetDamage(float damage)
        {
            itemDamage = damage;
        }

        public virtual void UseItem(LivingEntity who, ActionType actionType)
        {
        }

        public virtual void ClickItem(IInventoryView inventoryView)
        {
        }

        public int CompareTo(ItemStack other)
        {
            if (ReferenceEquals(this, other)) return 0;
            if (ReferenceEquals(null, other)) return 1;
            var itemNameComparison = string.Compare(itemName, other.itemName, StringComparison.Ordinal);
            if (itemNameComparison != 0) return itemNameComparison;
            return itemDamage.CompareTo(other.itemDamage);
        }
    }
}