﻿using System.Collections.Generic;
using Singleton;
using UnityEngine;

namespace Core.inventory
{
    public class ItemStackInstance : ResourceSingleton<ItemStackInstance>
    {
        [SerializeField] private ItemStack defaultItem;

        private Dictionary<ItemType, ItemStack> _itemStacksPrefab;

        public override void Awake()
        {
            base.Awake();
            Debug.Assert(defaultItem != null, "defaultItem can't be null!");

            InitInventoryItemIcon();
        }

        private void InitInventoryItemIcon()
        {
            _itemStacksPrefab = new Dictionary<ItemType, ItemStack>()
            {
                {ItemType.Default, defaultItem},
            };
        }

        public static ItemStack GetItemStack(ItemType itemType)
        {
            return Instantiate(Instance._itemStacksPrefab[itemType]);
        }
    }
}