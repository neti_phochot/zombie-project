﻿using Core.entity;
using Core.gameevent;

namespace Core.inventory
{
    public class PlayerInventory : Inventory
    {
        public void SetItemInMainHand(ItemStack itemStack)
        {
            SetItem(0, itemStack);
            EventInstance.Event.OnInventoryPickUpItemEvent(itemStack);
        }

        public ItemStack GetItemInMainHand()
        {
            return GetItem(0);
        }

        public new HumanEntity GetHolder()
        {
            return (HumanEntity) base.GetHolder();
        }

        public PlayerInventory(IInventoryHolder inventoryHolder, int inventorySize, InventoryType inventoryType) : base(
            inventoryHolder, inventorySize, inventoryType)
        {
        }
    }
}