﻿namespace Core.inventory.meta
{
    public interface IDamageable
    {
        float GetDamage();
        bool HasDamage();
        void SetDamage(float damage);
    }
}