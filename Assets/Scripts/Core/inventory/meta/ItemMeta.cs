﻿namespace Core.inventory.meta
{
    public interface IItemMeta
    {
        string GetDisplayName();
        void SetDisplayName(string name);
        string GetLore();
        void SetLore(string lore);
    }
}