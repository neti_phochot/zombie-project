﻿namespace Core.loot
{
    public interface ILootAble
    {
        int GetDropMoney();
    }
}