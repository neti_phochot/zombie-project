﻿using Core.entity;

namespace Core.projectile
{
    public interface IProjectile
    {
        Entity GetShooter();
        float GetDamage();
    }
}