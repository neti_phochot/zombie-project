﻿using Core.entity;
using UnityEngine;
using UnityEngine.Events;

namespace Core.projectile
{
    [RequireComponent(typeof(Rigidbody))]
    public class Projectile : Entity, IProjectile
    {
        private Rigidbody _rigidbody;
        private Entity _shooter;
        private float _damage;
        public event UnityAction<Projectile> ProjectileHitEvent;
        public event UnityAction<Projectile, Entity> ProjectileHitEntityEvent;

        protected override void Awake()
        {
            base.Awake();
            _rigidbody = GetComponent<Rigidbody>();
        }

        public void Init(Entity shooter, float damage, Vector3 velocity)
        {
            _shooter = shooter;
            _damage = damage;

            _rigidbody.AddForce(velocity, ForceMode.VelocityChange);
        }

        private void FixedUpdate()
        {
            var velocity = _rigidbody.velocity;
            if (velocity.magnitude > 0f)
                SetRotation(Quaternion.LookRotation(velocity.normalized));
        }

        private void OnCollisionEnter(Collision other)
        {
            Destroy(gameObject);

            if (other.transform.TryGetComponent<Entity>(out var entity))
            {
                entity.Damage(GetDamage());
                ProjectileHitEntityEvent?.Invoke(this, entity);
            }
            else
            {
                ProjectileHitEvent?.Invoke(this);
            }
        }

        public Entity GetShooter() => _shooter;
        public float GetDamage() => _damage;
    }
}