﻿using System.Collections.Generic;
using Core.entity;
using Singleton;
using UnityEngine;

namespace Core.projectile
{
    public class ProjectileSourceInstance : ResourceSingleton<ProjectileSourceInstance>
    {
        [SerializeField] private Projectile arrow;

        private Dictionary<ProjectileType, Projectile> _projectilesPrefab;

        public override void Awake()
        {
            base.Awake();
            Debug.Assert(arrow != null, "arrow can't be null!");

            InitProjectile();
        }

        private void InitProjectile()
        {
            _projectilesPrefab = new Dictionary<ProjectileType, Projectile>
            {
                {ProjectileType.Arrow, arrow}
            };
        }

        public static Projectile LaunchProjectile(
            Entity shooter, float damage, Vector3 velocity, ProjectileType projectileType)
        {
            var projectile = Instantiate(Instance._projectilesPrefab[projectileType], Vector3.zero,
                Quaternion.LookRotation(velocity.normalized));
            projectile.Init(shooter, damage, velocity);
            return projectile;
        }
    }
}