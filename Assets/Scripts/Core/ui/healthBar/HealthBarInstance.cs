﻿using System.Collections.Generic;
using Core.entity;
using Singleton;
using UnityEngine;

namespace Core.ui.healthBar
{
    public class HealthBarInstance : ResourceSingleton<HealthBarInstance>
    {
        [SerializeField] private HealthBarWorldUI defaultHealthBarWorldUI;

        private Dictionary<HealthBarType, HealthBarWorldUI> _healthBars;

        public override void Awake()
        {
            base.Awake();
            InitHealthBar();
        }

        private void InitHealthBar()
        {
            _healthBars = new Dictionary<HealthBarType, HealthBarWorldUI>()
            {
                {HealthBarType.Default, defaultHealthBarWorldUI}
            };
        }

        public static HealthBarWorldUI SetHealthBar(Entity entity, HealthBarType healthBarType, Vector3 offset)
        {
            var healthBar = Instantiate(Instance._healthBars[healthBarType], entity.transform);
            healthBar.Init(entity);
            healthBar.transform.localPosition = offset;
            return healthBar;
        }
    }
}