﻿using UnityEngine;
using UnityEngine.UI;

namespace Core.ui.healthBar
{
    public class HealthBarProvider : MonoBehaviour, IHealthBar
    {
        [SerializeField] private Transform healthBarTransform;
        [SerializeField] private Image healthBarImage;

        private void Awake()
        {
            healthBarTransform.gameObject.SetActive(false);
        }

        public void SetHealth(float health, float maxHealth)
        {
            var healthBar = health / maxHealth;
            healthBarImage.fillAmount = healthBar;
            healthBarTransform.gameObject.SetActive(healthBar < 1f);
        }
    }
}