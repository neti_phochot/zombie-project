﻿using Core.entity;
using UnityEngine;

namespace Core.ui.healthBar
{
    public class HealthBarWorldUI : MonoBehaviour
    {
        private IHealthBar _healthBar;

        private void Awake()
        {
            _healthBar = GetComponent<IHealthBar>();
            GetComponent<Canvas>().worldCamera = Camera.main;
        }

        public void Init(Entity entity)
        {
            entity.EntityDamageEvent += OnEntityDamageEvent;
        }

        private void OnEntityDamageEvent(Entity entity, float damage)
        {
            _healthBar.SetHealth(entity.GetHealth(), entity.GetMaxHealth());
        }
    }
}