﻿namespace Core.ui.healthBar
{
    public interface IHealthBar
    {
        void SetHealth(float health, float maxHealth);
    }
}