﻿using Core.inventory;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Core.ui.inventory
{
    public class ItemSlot : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
    {
        [SerializeField] private Image icon;

        private ItemStack _itemStack;
        public event UnityAction<ItemStack> OnClick;

        private void Awake()
        {
            Debug.Assert(icon != null, "icon can't be null!");
        }

        public void Init(ItemStack itemStack)
        {
            _itemStack = itemStack;
            icon.sprite = _itemStack.ItemIcon;
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            OnClick?.Invoke(_itemStack);
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            if (!HasHoverIcon()) return;
            icon.sprite = _itemStack.ItemHoverIcon;
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            if (!HasHoverIcon()) return;
            icon.sprite = _itemStack.ItemIcon;
        }

        private bool HasHoverIcon()
        {
            return _itemStack.ItemHoverIcon;
        }
    }
}