﻿using System.Collections.Generic;
using Core.entity;
using Core.entity.mob;
using Core.gameevent;
using Singleton;
using UnityEngine;

namespace Core.world
{
    public class WorldInstance : ResourceSingleton<WorldInstance>
    {
        [SerializeField] private Zombie zombie;
        [SerializeField] private DummyZombie dummyZombie;

        private Dictionary<EntityType, LivingEntity> _entitiesPrefab;

        public override void Awake()
        {
            base.Awake();
            InitEntities();
        }

        private void InitEntities()
        {
            _entitiesPrefab = new Dictionary<EntityType, LivingEntity>
            {
                {EntityType.Zombie, zombie},
                {EntityType.DummyZombie, dummyZombie},
            };
        }

        public static LivingEntity SpawnEntity(EntityType entityType, Vector3 location)
        {
            return Instantiate(Instance._entitiesPrefab[entityType], location, Quaternion.identity);
        }
    }
}