﻿using UnityEngine;

namespace Player
{
    public class PlayerController : MonoBehaviour
    {
        public enum PlayerState
        {
            Idle,
            Duck,
            Walking,
            Jumping,
            Sprinting
        }
        public float SpeedModifier { get; set; }

        [Header("World Settings")]
        public float gravity = -9.81f;

        [Header("Movement Settings")]
        public float walkSpeed = 6f;
        public float duckSpeed = 3f;
        public float sprintSpeed = 9f;
        public float jumpForce = 1.3f;

        [Range(0, 1)] public float divideSpeedInTheAir = 0.45f;

        private CharacterController _characterController;

        private float _horizontal;
        private float _vertical;

        private float _velocityMomentum;
        private Vector3 _velocity;

        private bool _isJumping;
        private bool _isDuck;
        private bool _isSprinting;


        #region Property Player
        public CharacterController GetCharacterController()
        {
            return _characterController;
        }

        public void SetLocation(Vector3 loc)
        {
            _characterController.enabled = false;
            transform.position = loc;
            _characterController.enabled = true;
        }

        public PlayerState GetPlayerState()
        {
            if (_isDuck)
                return PlayerState.Duck;

            if (_characterController.velocity == Vector3.zero)
                return PlayerState.Idle;

            if (_isJumping)
                return PlayerState.Jumping;

            if (_isSprinting)
                return PlayerState.Sprinting;

            return PlayerState.Walking;
        }
        public float GetMoveSpeed()
        {
            float speed;

            if (_isDuck && _characterController.isGrounded)
                speed = duckSpeed;

            else if (_isSprinting)
                speed = sprintSpeed;

            else speed = walkSpeed;
            if (SpeedModifier > 0)
                speed *= SpeedModifier;

            return speed;
        }

        #endregion Property Player
        private void Awake()
        {
            GetComponent();
        }

        private void GetComponent()
        {
            _characterController = GetComponent<CharacterController>();
        }

        private void FixedUpdate()
        {
            CalculateVelocity();
        }

        private void Update()
        {
            PlayerInput();
            Movement();
        }

        #region Movement
        private void PlayerInput()
        {
            //Movement
            _vertical = Input.GetAxis("Vertical");
            _horizontal = Input.GetAxis("Horizontal");

            _isJumping = Input.GetKey(KeyCode.Space);
            _isDuck = Input.GetKey(KeyCode.LeftShift);

            if (Input.GetKeyDown(KeyCode.LeftControl))
                _isSprinting = true;
        }

        private void CalculateVelocity()
        {
            //Decrease velocity when player not on the ground
            if (!_characterController.isGrounded)
            {
                _horizontal *= divideSpeedInTheAir;

                if (_vertical < 0f)
                    _vertical *= divideSpeedInTheAir;
            }

            var transform1 = transform;
            _velocity = (transform1.forward * _vertical) + (transform1.right * _horizontal);

            //Apply Jump Force
            if (_isJumping && _characterController.isGrounded)
                _velocityMomentum = jumpForce;

            //Apply Gravity
            if (_velocityMomentum > gravity)
                _velocityMomentum += gravity * Time.fixedDeltaTime;

            //Apply Velocity Momentum (Jumping/Falling)
            _velocity += Vector3.up * _velocityMomentum;
        }

        private void Movement()
        {
            //Toggle Walking/Sprint
            if (GetPlayerState() == PlayerState.Idle || GetPlayerState() == PlayerState.Duck)
                _isSprinting = false;

            //Move Character
            _characterController.Move(_velocity * (Time.deltaTime * GetMoveSpeed()));

        }
        #endregion Movement
    }
}
