﻿using Core.inventory;
using UnityEngine;

namespace Player
{
    [RequireComponent(typeof(global::Core.entity.Player))]
    public class PlayerInteract : MonoBehaviour
    {
        private global::Core.entity.Player _player;

        private void Awake()
        {
            _player = GetComponent<global::Core.entity.Player>();
        }

        private void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                _player.UseItem(_player.GetInventory().GetItemInMainHand(), ActionType.UseItemDown);
            }

            if (Input.GetMouseButtonUp(0))
            {
                _player.UseItem(_player.GetInventory().GetItemInMainHand(), ActionType.UseItemUp);
            }

            if (Input.GetMouseButtonDown(1))
            {
                _player.UseItem(_player.GetInventory().GetItemInMainHand(), ActionType.ConsumeItemDown);
            }

            if (Input.GetMouseButtonUp(1))
            {
                _player.UseItem(_player.GetInventory().GetItemInMainHand(), ActionType.ConsumeItemUp);
            }

            if (Input.GetKeyDown(KeyCode.E))
            {
                _player.TryInteract();
            }

            if (Input.GetKeyDown(KeyCode.Tab))
            {
                var openInventory = _player.GetOpenInventory();
                if (openInventory != null)
                {
                    _player.CloseInventory();
                }
                else
                {
                    _player.OpenInventory(_player.GetInventory());
                }
            }
        }
    }
}