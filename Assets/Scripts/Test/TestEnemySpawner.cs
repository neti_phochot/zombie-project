using System.Collections.Generic;
using Core.entity;
using Core.ui.healthBar;
using Core.world;
using UnityEngine;

namespace Test
{
    public class TestEnemySpawner : MonoBehaviour
    {
        private readonly List<Creature> _monsters = new List<Creature>();

#if UNITY_EDITOR
        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Z))
            {
                if (!Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward,
                    out var hit)) return;
                var creator = (Creature) WorldInstance.SpawnEntity(EntityType.Zombie, hit.point);
                var offset = creator.TargetProvider.GetEyesTransform().localPosition + new Vector3(0, 1);
                HealthBarInstance.SetHealthBar(creator, HealthBarType.Default, offset);
                _monsters.Add(creator);
            }

            if (Input.GetKeyDown(KeyCode.X))
            {
                if (!Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward,
                    out var hit)) return;
                foreach (var monster in _monsters)
                {
                    if (!monster) continue;
                    var randomArea = Random.insideUnitCircle;
                    var area = new Vector3(randomArea.x, 0, randomArea.y);
                    area *= 3f;
                    monster.SetDestination(hit.point + area);
                }
            }
        }

#endif
    }
}