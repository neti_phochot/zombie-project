﻿using Core.entity;
using UnityEngine;
using UnityEngine.Events;

namespace TowerDefense
{
    public class DefencePoint : MonoBehaviour
    {
        public event UnityAction<LivingEntity> EntityEnterEvent;

        private void OnTriggerEnter(Collider other)
        {
            if (!other.TryGetComponent<Monster>(out var enemy)) return;
            Destroy(enemy.gameObject);
            EntityEnterEvent?.Invoke(enemy);
        }
    }
}