﻿namespace TowerDefense.entity.tower
{
    public interface ITradAble
    {
        int GetBuyPrice();
        int GetSellPrice();
    }
}