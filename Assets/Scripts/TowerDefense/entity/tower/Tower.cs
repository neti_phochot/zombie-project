﻿using Core.entity;
using Core.inventory;
using TowerDefense.inventory;
using TowerDefense.inventory.item.tower;
using UnityEngine;

namespace TowerDefense.entity.tower
{
    public abstract class Tower : LivingEntity, IInventoryHolder, ITradAble
    {
        [Header("TOWER")] [SerializeField] private TowerType[] upgradeTower;
        [SerializeField] private int buyAmount;
        [SerializeField] private int sellAmount;

        private Inventory _inventory;

        public Inventory GetInventory()
        {
            return _inventory;
        }

        protected override void Awake()
        {
            base.Awake();

            Debug.Assert(upgradeTower.Length > 0, "upgradeTower can't be null.");
            Init();
        }

        private void Init()
        {
            if (upgradeTower.Length < 1)
            {
                Debug.LogWarning("No Tower Command Item Found!");
                return;
            }

            _inventory = new Inventory(this, upgradeTower.Length, InventoryType.Default);
            foreach (var towerType in upgradeTower)
            {
                var item = TowerCommandItemInstance.GetTowerItem(towerType);
                item.Init(this);
                _inventory.AddItem(item);
            }
        }

        public override void Interact(Core.entity.Player player)
        {
            var inventory = player.GetOpenInventory();
            if (inventory != null)
            {
                player.CloseInventory();
            }
            else
            {
                player.OpenInventory(_inventory);
            }
        }

        public int GetBuyPrice() => buyAmount;
        public int GetSellPrice() => sellAmount;
    }
}