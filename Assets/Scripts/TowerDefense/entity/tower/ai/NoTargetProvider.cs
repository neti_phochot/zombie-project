﻿using Core.entity;
using Core.entity.ai;
using UnityEngine;

namespace TowerDefense.entity.tower.ai
{
    public class NoTargetProvider : MonoBehaviour, IEntityTargetProvider
    {
        public Entity GetLookingEntity() => null;
        public Vector3 GetLookingLocation() => Vector3.zero;
        public Transform GetEyesTransform() => transform;
        public Vector3 GetDirection() => Vector3.zero;
    }
}