﻿using Core.inventory;
using TowerDefense.inventory.item.weapon;
using UnityEngine;

namespace TowerDefense.entity.tower.ai
{
    [RequireComponent(typeof(Tower))]
    public class TowerAI : MonoBehaviour
    {
        [SerializeField] private GunType gunType;
        private Tower _tower;
        private Gun _weapon;

        private void Awake()
        {
            _tower = GetComponent<Tower>();
            InitTowerWeapon();
        }

        private void InitTowerWeapon()
        {
            _weapon = GunInstance.GetGun(gunType);
            Transform transform1;
            (transform1 = _weapon.transform).SetParent(_tower.TargetProvider.GetEyesTransform());
            transform1.localPosition = Vector3.zero;
        }

        private void FixedUpdate()
        {
            AttackTarget();
        }

        private void AttackTarget()
        {
            _weapon.UseItem(_tower,
                _tower.TargetProvider.GetLookingEntity() ? ActionType.UseItemDown : ActionType.UseItemUp);
        }
    }
}