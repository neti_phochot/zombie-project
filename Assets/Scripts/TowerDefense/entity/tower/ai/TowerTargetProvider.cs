﻿using Core.entity;
using Core.entity.ai;
using TowerDefense.game;
using UnityEngine;

namespace TowerDefense.entity.tower.ai
{
    public class TowerTargetProvider : MonoBehaviour, IEntityTargetProvider
    {
        [SerializeField] private float findTargetRadius;
        [SerializeField] private Transform headTransform;
        [SerializeField] private float rotateSpeed;

        private LivingEntity _lookingEntity;
        private GameManager _gameManager;

        private void Awake()
        {
            _gameManager = FindObjectOfType<GameManager>();
        }

        private void FixedUpdate()
        {
            FindTarget();
            RotateToTarget();
        }

        private void FindTarget()
        {
            if (_lookingEntity)
            {
                if (!InRange(_lookingEntity.GetLocation()) || _lookingEntity.IsDead)
                    _lookingEntity = null;
            }

            foreach (var zombie in _gameManager.WaveManager.GetWaveSpawner().GetSpawnedEntities())
            {
                if (!zombie) continue;
                if (!InRange(zombie.transform.position)) continue;
                _lookingEntity = zombie;
            }
        }

        private bool InRange(Vector3 target)
        {
            return Vector3.Distance(headTransform.position, target) < findTargetRadius;
        }

        private void RotateToTarget()
        {
            if (!_lookingEntity) return;
            var transform1 = _lookingEntity.TargetProvider.GetEyesTransform();
            var speed = _lookingEntity.GetVelocity().normalized;
            var targetPosition = transform1.position + speed;
            var dirToTarget = (targetPosition - headTransform.position).normalized;
            dirToTarget.y = 0f;
            headTransform.rotation = Quaternion.Lerp(headTransform.rotation, Quaternion.LookRotation(dirToTarget),
                Time.deltaTime * rotateSpeed);
        }

        public Entity GetLookingEntity() => _lookingEntity;

        public Vector3 GetLookingLocation()
        {
            return _lookingEntity ? _lookingEntity.GetLocation() : Vector3.zero;
        }

        public Transform GetEyesTransform() => headTransform;
        public Vector3 GetDirection() => headTransform.forward;

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(headTransform.position, findTargetRadius);
        }
    }
}