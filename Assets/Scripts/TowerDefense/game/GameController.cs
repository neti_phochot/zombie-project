﻿using Core.entity;
using TowerDefense.gameevent;

namespace TowerDefense.game
{
    public class GameController : BaseListener
    {
        private GameManager _gameManager;

        private void Awake()
        {
            _gameManager = FindObjectOfType<GameManager>();
        }

        protected override void RegisterEvents()
        {
            TDEventInstance.GameEvent.WaveTimerEndEvent += OnWaveTimerEndEvent;
            TDEventInstance.GameEvent.EnemyEnterDefencePointEvent += OnEnemyEnterDefencePointEvent;
            TDEventInstance.GameEvent.KillEnemyEvent += OnKillEnemyEvent;
            TDEventInstance.GameEvent.WaveClearedEvent += OnWaveClearedEvent;
        }

        private void OnWaveTimerEndEvent()
        {
            //AUTO START NEXT WAVE
            if (_gameManager.WaveManager.IsFinalWave()) return;
            _gameManager.StartGame();
        }

        private void OnEnemyEnterDefencePointEvent()
        {
            //CHECK LOSS
            _gameManager.HealthPoint.AddHealth(-1);
            if (_gameManager.HealthPoint.Health > 0) return;
            _gameManager.SetPlayerWin(false);
        }

        private void OnKillEnemyEvent(Monster monster)
        {
            _gameManager.PlayerCoin.AddCoin(monster.GetDropMoney());
        }

        private void OnWaveClearedEvent()
        {
            //CHECK WIN
            if (!_gameManager.WaveManager.IsFinalWave()) return;
            _gameManager.SetPlayerWin(true);
        }
    }
}