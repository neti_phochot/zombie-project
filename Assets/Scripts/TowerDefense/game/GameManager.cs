﻿using TowerDefense.wave;
using UnityEngine;
using UnityEngine.Events;

namespace TowerDefense.game
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private int healthPoint;
        public event UnityAction GameStartEvent;
        public event UnityAction<bool> GameOverEvent;
        public IWaveManager WaveManager { get; private set; }
        public PlayerCoin PlayerCoin { get; } = new PlayerCoin();
        public HealthPoint HealthPoint { get; } = new HealthPoint();

        public void Awake()
        {
            WaveManager = GetComponent<IWaveManager>();
            Debug.Assert(WaveManager != null, "IWaveManager!=null");
        }

        private void Start()
        {
            HealthPoint.AddHealth(healthPoint);
            PlayerCoin.AddCoin(10);
        }

        public void StartGame()
        {
            WaveManager.NextWave();
            GameStartEvent?.Invoke();

            Debug.Log("GAME START!");
        }

        public void SetPlayerWin(bool win)
        {
            GameOverEvent?.Invoke(win);
            var result = win ? "WIN" : "LOSS";
            Debug.Log($"YOU {result}!");
        }
    }
}