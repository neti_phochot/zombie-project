﻿using UnityEngine.Events;

namespace TowerDefense.game
{
    public class HealthPoint
    {
        public event UnityAction<int, int> HealthPointChangeEvent;
        public int Health { get; private set; }

        public void AddHealth(int amount)
        {
            var newHealth = Health + amount;
            HealthPointChangeEvent?.Invoke(Health, newHealth);
            Health = newHealth;
        }
    }
}