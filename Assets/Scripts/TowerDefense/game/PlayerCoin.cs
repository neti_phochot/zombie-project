﻿using UnityEngine.Events;

namespace TowerDefense.game
{
    public class PlayerCoin
    {
        public event UnityAction<int, int> CoinChangedEvent;
        public int Coin { get; private set; }

        public void AddCoin(int amount)
        {
            var newAmount = Coin + amount;
            CoinChangedEvent?.Invoke(amount, newAmount);
            Coin = newAmount;
        }
    }
}