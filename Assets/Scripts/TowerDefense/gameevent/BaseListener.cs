﻿using UnityEngine;

namespace TowerDefense.gameevent
{
    public abstract class BaseListener : MonoBehaviour
    {
        private void OnEnable()
        {
            RegisterEvents();
        }

        protected abstract void RegisterEvents();
    }
}