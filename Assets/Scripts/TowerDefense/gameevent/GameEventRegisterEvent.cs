﻿using System.Linq;
using Core.entity;
using Core.gameevent;
using Core.inventory;
using TowerDefense.game;
using TowerDefense.inventory.item.weapon;
using UnityEngine;

namespace TowerDefense.gameevent
{
    [RequireComponent(typeof(GameManager))]
    public class GameEventRegisterEvent : BaseRegisterEvent
    {
        private GameManager _gameManager;

        protected override void RegisterEvents()
        {
            _gameManager = GetComponent<GameManager>();

            //Player
            _gameManager.PlayerCoin.CoinChangedEvent += OnCoinChangedEvent;
            _gameManager.HealthPoint.HealthPointChangeEvent += OnHealthPointChangeEvent;

            //GameManager
            _gameManager.GameStartEvent += OnGameStartEvent;
            _gameManager.GameOverEvent += OnGameOverEvent;

            //WaveManger
            _gameManager.WaveManager.WaveChangedEvent += OnWaveChangedEvent;
            _gameManager.WaveManager.FinalWaveEvent += OnFinalWaveEvent;
            _gameManager.WaveManager.WaveTimerTickEvent += OnWaveTimerTickEvent;
            _gameManager.WaveManager.WaveTimerEndEvent += OnWaveTimerEndEvent;

            //WaveSpawner
            _gameManager.WaveManager.GetWaveSpawner().KillEnemyEvent += OnKillEnemyEvent;
            _gameManager.WaveManager.GetWaveSpawner().WaveClearedEvent += OnWaveClearedEvent;


            //DefencePoint
            foreach (var defencePoint in FindObjectsOfType<DefencePoint>())
                defencePoint.EntityEnterEvent += OnEntityEnterEvent;

            //Weapon
            EventInstance.Event.InventoryClickEvent += OnInventoryClickEvent;
            EventInstance.Event.InventoryPickUpItemEvent += CheckWeapon;
        }

        private void OnInventoryClickEvent(IInventoryView inventoryView)
        {
            CheckWeapon(inventoryView.GetCursor());
        }

        private void CheckWeapon(ItemStack itemStack)
        {
            if (!(itemStack is IGun gun)) return;
            TDEventInstance.GameEvent.OnWeaponSwitchEvent(gun);
        }

        private void OnKillEnemyEvent(Monster monster)
        {
            TDEventInstance.GameEvent.OnKillEnemyEvent(monster);
        }

        private void OnEntityEnterEvent(LivingEntity livingEntity)
        {
            if (!_gameManager.WaveManager.GetWaveSpawner().GetSpawnedEntities().Contains(livingEntity)) return;
            TDEventInstance.GameEvent.OnEnemyEnterDefencePointEvent();
        }

        private void OnWaveTimerTickEvent(float time)
        {
            TDEventInstance.GameEvent.OnWaveTimerTickEvent(time);
        }

        private void OnWaveTimerEndEvent()
        {
            TDEventInstance.GameEvent.OnWaveTimerEndEvent();
        }

        private void OnCoinChangedEvent(int previousAmount, int newAmount)
        {
            TDEventInstance.GameEvent.OnCoinChangedEvent(previousAmount, newAmount);
        }

        private void OnHealthPointChangeEvent(int previousHealth, int newHealth)
        {
            TDEventInstance.GameEvent.OnHealthPointChangedEvent(previousHealth, newHealth);
        }

        #region GAME

        private void OnGameStartEvent()
        {
            TDEventInstance.GameEvent.OnGameStartEvent();
        }

        private void OnGameOverEvent(bool win)
        {
            TDEventInstance.GameEvent.OnGameOverEvent(win);
        }

        #endregion

        #region WAVE

        private void OnWaveChangedEvent(int wave, int maxWave)
        {
            TDEventInstance.GameEvent.OnWaveChangedEvent(wave, maxWave);
        }

        private void OnFinalWaveEvent()
        {
            TDEventInstance.GameEvent.OnFinalWaveEvent();
        }

        private void OnWaveClearedEvent()
        {
            TDEventInstance.GameEvent.OnWaveCompletedEvent();
        }

        #endregion
    }
}