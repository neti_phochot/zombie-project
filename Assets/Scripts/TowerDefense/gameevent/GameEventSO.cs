﻿using Core.entity;
using TowerDefense.inventory.item.weapon;
using UnityEngine;
using UnityEngine.Events;

namespace TowerDefense.gameevent
{
    [CreateAssetMenu(menuName = "TD/GameEvent")]
    public class GameEventSO : ScriptableObject
    {
        public event UnityAction<int, int> CoinChangedEvent;
        public event UnityAction<int, int> HealthPointChangedEvent;
        public event UnityAction GameStartEvent;
        public event UnityAction<bool> GameOverEvent;
        public event UnityAction<int, int> WaveChangedEvent;
        public event UnityAction FinalWaveEvent;
        public event UnityAction<float> WaveTimerTickEvent;
        public event UnityAction WaveTimerEndEvent;
        public event UnityAction<Monster> KillEnemyEvent;
        public event UnityAction WaveClearedEvent;
        public event UnityAction EnemyEnterDefencePointEvent;
        public event UnityAction<IGun> WeaponSwitchEvent;

        public void OnCoinChangedEvent(int previousAmount, int newAmount)
        {
            CoinChangedEvent?.Invoke(previousAmount, newAmount);
        }

        public void OnGameStartEvent()
        {
            GameStartEvent?.Invoke();
        }

        public void OnGameOverEvent(bool win)
        {
            GameOverEvent?.Invoke(win);
        }

        public void OnWaveChangedEvent(int wave, int maxWave)
        {
            WaveChangedEvent?.Invoke(wave, maxWave);
        }

        public void OnFinalWaveEvent()
        {
            FinalWaveEvent?.Invoke();
        }

        public void OnWaveCompletedEvent()
        {
            WaveClearedEvent?.Invoke();
        }

        public void OnWaveTimerTickEvent(float time)
        {
            WaveTimerTickEvent?.Invoke(time);
        }

        public void OnEnemyEnterDefencePointEvent()
        {
            EnemyEnterDefencePointEvent?.Invoke();
        }

        public void OnWaveTimerEndEvent()
        {
            WaveTimerEndEvent?.Invoke();
        }

        public void OnHealthPointChangedEvent(int previousHealth, int newHealth)
        {
            HealthPointChangedEvent?.Invoke(previousHealth, newHealth);
        }

        public void OnKillEnemyEvent(Monster monster)
        {
            KillEnemyEvent?.Invoke(monster);
        }

        public void OnWeaponSwitchEvent(IGun gun)
        {
            WeaponSwitchEvent?.Invoke(gun);
        }
    }
}