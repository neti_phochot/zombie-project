﻿using Singleton;
using UnityEngine;

namespace TowerDefense.gameevent
{
    public class TDEventInstance : ResourceSingleton<TDEventInstance>
    {
        [SerializeField] private GameEventSO gameEventSo;
        public static GameEventSO GameEvent => Instance.gameEventSo;
    }
}