﻿using UnityEngine;
using UnityEngine.Events;

namespace TowerDefense.gameevent
{
    [CreateAssetMenu(menuName = "TD/WeaponEvent")]
    public class WeaponEventSO : ScriptableObject
    {
        public event UnityAction<int, int> CoinChangedEvent;
    }
}