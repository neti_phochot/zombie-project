﻿using Core.inventory;
using TowerDefense.entity.tower;
using TowerDefense.world.tower;
using UnityEngine;

namespace TowerDefense.inventory.item.tower
{
    public class TowerCommandItem : ItemStack
    {
        [Header("TOWER")] [SerializeField] private TowerType spawnTowerType;

        private Tower _tower;

        public void Init(Tower towerToReplace)
        {
            _tower = towerToReplace;
            transform.SetParent(towerToReplace.transform);
        }

        public override void ClickItem(IInventoryView inventoryView)
        {
            inventoryView.GetPlayer().CloseInventory();
            var oldTransform = _tower.transform;
            var tower = TowerInstance.GetTower(spawnTowerType);
            tower.SetLocation(oldTransform.position);
            tower.SetRotation(oldTransform.rotation);
            Destroy(_tower.gameObject);
        }
    }
}