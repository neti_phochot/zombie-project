﻿using System.Collections.Generic;
using Singleton;
using UnityEngine;

namespace TowerDefense.inventory.item.tower
{
    public class TowerCommandItemInstance : ResourceSingleton<TowerCommandItemInstance>
    {
        [SerializeField] private TowerCommandItem defaultTowerItem;
        [SerializeField] private TowerCommandItem fastTowerItem;
        [SerializeField] private TowerCommandItem slowTowerItem;
        [SerializeField] private TowerCommandItem protectTowerItem;
        [SerializeField] private TowerCommandItem heavyTowerItem;

        private Dictionary<TowerType, TowerCommandItem> _itemStacksPrefab;

        public override void Awake()
        {
            base.Awake();
            Debug.Assert(defaultTowerItem != null, "defaultTowerItem can't be null!");
            Debug.Assert(fastTowerItem != null, "fastTowerItem can't be null!");
            Debug.Assert(slowTowerItem != null, "slowTowerItem can't be null!");
            Debug.Assert(protectTowerItem != null, "protectTowerItem can't be null!");
            Debug.Assert(heavyTowerItem != null, "heavyTowerItem can't be null!");

            InitInventoryItemIcon();
        }

        private void InitInventoryItemIcon()
        {
            _itemStacksPrefab = new Dictionary<TowerType, TowerCommandItem>()
            {
                {TowerType.Default, defaultTowerItem},
                {TowerType.Fast, fastTowerItem},
                {TowerType.Slow, slowTowerItem},
                {TowerType.Protect, protectTowerItem},
                {TowerType.Heavy, heavyTowerItem},
            };
        }

        public static TowerCommandItem GetTowerItem(TowerType towerType)
        {
            return Instantiate(Instance._itemStacksPrefab[towerType]);
        }
    }
}