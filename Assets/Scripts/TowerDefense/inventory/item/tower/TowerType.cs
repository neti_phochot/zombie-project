﻿namespace TowerDefense.inventory.item.tower
{
    public enum TowerType
    {
        Default,
        Fast,
        Slow,
        Protect,
        Heavy
    }
}