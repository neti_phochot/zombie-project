﻿using Core.entity;
using Core.inventory;
using Core.projectile;
using UnityEngine;
using UnityEngine.Events;
using Utils;
using player = Core.entity.Player;

namespace TowerDefense.inventory.item.weapon
{
    public abstract class Gun : ItemStack, IGun
    {
        [SerializeField] private GunSO gunSo;
        public event UnityAction<int, int> AmmoChangedEvent;
        public event UnityAction OutOfAmmoEvent;
        public event UnityAction<float> ReloadingEvent;
        public event UnityAction ReloadedEvent;

        private GameTimer _fireRateTimer;
        private GameTimer _reloadTimer;

        private bool _isTriggerDown;

        private LivingEntity _shooter;

        private int _ammo;
        private int _maxAmmo;

        public void ClearEvents()
        {
            AmmoChangedEvent = null;
            OutOfAmmoEvent = null;
            ReloadingEvent = null;
            ReloadedEvent = null;
        }

        public override void Awake()
        {
            base.Awake();

            _fireRateTimer = new GameTimer(gunSo.FireRate, gunSo.FireRate);
            _reloadTimer = new GameTimer(gunSo.ReloadTime, 0.02f);

            _reloadTimer.OnTimerTick += OnReloading;
            _reloadTimer.OnTimerEnd += OnReloaded;
        }

        private void Start()
        {
            InitGun();
        }

        private void InitGun()
        {
            SetDisplayName(gunSo.WeaponName);
            _maxAmmo = gunSo.MaxAmmo;
            FillAmmo();
        }

        private void Update()
        {
            //if (Input.GetKeyDown(KeyCode.R)) Reload();
            CheckTrigger();

            _fireRateTimer.UpdateTimer();
            _reloadTimer.UpdateTimer();
        }

        public override void UseItem(LivingEntity who, ActionType actionType)
        {
            _shooter = who;
            switch (actionType)
            {
                case ActionType.UseItemDown:
                    _isTriggerDown = true;
                    break;
                case ActionType.UseItemUp:
                    _isTriggerDown = false;
                    break;
                case ActionType.ConsumeItemDown:
                    Interact(who, true);
                    break;
                case ActionType.ConsumeItemUp:
                    Interact(who, false);
                    break;
                default:
                    return;
            }
        }

        public void Shoot()
        {
            if (!IsReady()) return;
            if (_ammo < 1)
            {
                OutOfAmmoEvent?.Invoke();
                _isTriggerDown = false;
                return;
            }

            if (!gunSo.IsUnlimitedAmmo) _ammo--;
            _fireRateTimer.StartTimer(gunSo.FireRate);

            var direction = _shooter.TargetProvider.GetDirection();
            var velocity = direction * gunSo.Speed;
            var shootLocation = _shooter.TargetProvider.GetEyesTransform().position + direction;
            var projectile =
                ProjectileSourceInstance.LaunchProjectile(_shooter, gunSo.Damage, velocity, gunSo.ProjectileType);
            projectile.SetLocation(shootLocation);

            AmmoChangedEvent?.Invoke(_ammo, _maxAmmo);
        }

        protected virtual void Interact(LivingEntity shooter, bool triggerDown)
        {
        }

        public void Reload()
        {
            if (!IsReady())
            {
                Debug.LogWarning("ALREADY RELOADING...");
                return;
            }

            if (IsFullAmmo())
            {
                Debug.LogWarning("FULL AMMO!");
                return;
            }

            Debug.Log("RELOADING...");
            _reloadTimer.StartTimer(gunSo.ReloadTime);
        }

        public void FillAmmo()
        {
            _ammo = gunSo.MaxAmmo;
            AmmoChangedEvent?.Invoke(_ammo, _ammo);
        }

        public bool IsReady()
        {
            return !_fireRateTimer.IsRunning && !_reloadTimer.IsRunning;
        }

        public bool IsFullAmmo()
        {
            return _ammo == _maxAmmo;
        }

        public Sprite GetIcon() => ItemIcon;
        public int GetAmmo() => _ammo;
        public int GetMaxAmmo() => _maxAmmo;
        public float GetReloadTime() => gunSo.ReloadTime;

        private void CheckTrigger()
        {
            if (!_isTriggerDown) return;
            Shoot();
            if (gunSo.IsTrigger) _isTriggerDown = false;
        }

        private void OnReloading(float time)
        {
            ReloadingEvent?.Invoke(time);
        }

        private void OnReloaded()
        {
            FillAmmo();
            ReloadingEvent?.Invoke(gunSo.ReloadTime);
            ReloadedEvent?.Invoke();
        }
    }
}