﻿using System.Collections.Generic;
using Singleton;
using UnityEngine;

namespace TowerDefense.inventory.item.weapon
{
    public class GunInstance : ResourceSingleton<GunInstance>
    {
        [SerializeField] private Gun defaultGun;

        private Dictionary<GunType, Gun> _gunPrefabs;

        public override void Awake()
        {
            base.Awake();
            InitGun();
        }

        private void InitGun()
        {
            _gunPrefabs = new Dictionary<GunType, Gun>()
            {
                {GunType.Default, defaultGun}
            };
        }

        public static Gun GetGun(GunType gunType)
        {
            return Instantiate(Instance._gunPrefabs[gunType]);
        }
    }
}