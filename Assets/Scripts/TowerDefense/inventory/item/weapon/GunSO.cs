﻿using Core.projectile;
using UnityEngine;

namespace TowerDefense.inventory.item.weapon
{
    [CreateAssetMenu(fileName = "New Gun", menuName = "Weapon/Gun")]
    public class GunSO : ScriptableObject
    {
        [Header("WEAPON INFORMATION")] [SerializeField]
        private string weaponName;

        [Header("WEAPON SETTINGS")] [SerializeField]
        private bool isTrigger;

        [SerializeField] private bool isUnlimitedAmmo;
        [SerializeField] private ProjectileType projectileType;

        [SerializeField] private float damage;
        [SerializeField] private float speed;
        [SerializeField] private int maxAmmo;
        [SerializeField] private float fireRate;
        [SerializeField] private float reloadTime;

        public string WeaponName => weaponName;
        public ProjectileType ProjectileType => projectileType;
        public bool IsTrigger => isTrigger;
        public bool IsUnlimitedAmmo => isUnlimitedAmmo;
        public float Damage => damage;
        public float Speed => speed;
        public int MaxAmmo => maxAmmo;
        public float FireRate => fireRate;
        public float ReloadTime => reloadTime;
    }
}