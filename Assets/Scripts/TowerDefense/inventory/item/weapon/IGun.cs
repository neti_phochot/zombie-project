﻿using UnityEngine;
using UnityEngine.Events;

namespace TowerDefense.inventory.item.weapon
{
    public interface IGun
    {
        event UnityAction<int, int> AmmoChangedEvent;
        event UnityAction OutOfAmmoEvent;
        event UnityAction<float> ReloadingEvent;
        event UnityAction ReloadedEvent;
        void ClearEvents();
        void Shoot();
        void Reload();
        void FillAmmo();
        bool IsReady();
        bool IsFullAmmo();
        int GetAmmo();
        int GetMaxAmmo();
        float GetReloadTime();
        Sprite GetIcon();
    }
}