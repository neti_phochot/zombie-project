﻿using TowerDefense.gameevent;

namespace TowerDefense.ui
{
    public abstract class BaseUI : BaseListener, IBaseUI
    {
        public void Show()
        {
            gameObject.SetActive(true);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}