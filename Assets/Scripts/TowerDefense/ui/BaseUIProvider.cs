﻿using UnityEngine;

namespace TowerDefense.ui
{
    public abstract class BaseUIProvider : MonoBehaviour
    {
        public virtual void Show()
        {
            gameObject.SetActive(true);
        }

        public virtual void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}