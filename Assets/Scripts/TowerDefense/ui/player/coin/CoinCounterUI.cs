﻿using TowerDefense.gameevent;
using UnityEngine;

namespace TowerDefense.ui.player.coin
{
    public class CoinCounterUI : BaseUI
    {
        private ICoinCounter _coinCounter;

        private void Awake()
        {
            _coinCounter = GetComponent<ICoinCounter>();
            Debug.Assert(_coinCounter != null, "ICoinCounter!=null");
        }

        protected override void RegisterEvents()
        {
            TDEventInstance.GameEvent.CoinChangedEvent += OnCoinChangedEvent;
        }

        private void OnCoinChangedEvent(int previousCoin, int newCoin)
        {
            _coinCounter.SetCoinCounter(previousCoin, newCoin);
        }
    }
}