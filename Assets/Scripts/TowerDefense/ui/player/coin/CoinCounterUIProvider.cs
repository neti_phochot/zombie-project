﻿using TMPro;
using UnityEngine;

namespace TowerDefense.ui.player.coin
{
    public class CoinCounterUIProvider : BaseUIProvider, ICoinCounter
    {
        [SerializeField] private TextMeshProUGUI coinText;

        public void SetCoinCounter(int previousAmount, int newAmount)
        {
            coinText.text = $"{newAmount} Coins";
        }
    }
}