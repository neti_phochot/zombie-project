﻿namespace TowerDefense.ui.player.coin
{
    public interface ICoinCounter : IBaseUI
    {
        void SetCoinCounter(int previousAmount, int mewAmount);
    }
}