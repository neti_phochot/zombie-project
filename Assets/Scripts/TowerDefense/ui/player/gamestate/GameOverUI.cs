﻿using TowerDefense.gameevent;
using UnityEngine;

namespace TowerDefense.ui.player.gamestate
{
    public class GameOverUI : BaseUI
    {
        private IGameOver _gameOver;

        private void Awake()
        {
            _gameOver = GetComponent<IGameOver>();
            Debug.Assert(_gameOver != null, "IGameOver!=null");
        }

        protected override void RegisterEvents()
        {
            TDEventInstance.GameEvent.GameOverEvent += OnGameOverEvent;
        }

        private void OnGameOverEvent(bool win)
        {
            _gameOver.SetGameOver(win);
        }
    }
}