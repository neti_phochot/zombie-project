﻿namespace TowerDefense.ui.player.gamestate
{
    public interface IGameOver
    {
        void SetGameOver(bool win);
    }
}