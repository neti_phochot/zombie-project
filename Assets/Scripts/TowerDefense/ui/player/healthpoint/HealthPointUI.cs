﻿using TowerDefense.gameevent;
using UnityEngine;

namespace TowerDefense.ui.player.healthpoint
{
    public class HealthPointUI : BaseUI
    {
        private IHealthPoint _healthPoint;

        private void Awake()
        {
            _healthPoint = GetComponent<IHealthPoint>();
            Debug.Assert(_healthPoint != null, "IHealthPoint!=null");
        }

        protected override void RegisterEvents()
        {
            TDEventInstance.GameEvent.HealthPointChangedEvent += OnHealthPointChangedEvent;
        }

        private void OnHealthPointChangedEvent(int previousHealth, int newHealth)
        {
            _healthPoint.SetHealthPoint(previousHealth, newHealth);
        }
    }
}