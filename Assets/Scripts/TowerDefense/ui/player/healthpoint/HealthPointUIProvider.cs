﻿using TMPro;
using UnityEngine;

namespace TowerDefense.ui.player.healthpoint
{
    public class HealthPointUIProvider : BaseUIProvider, IHealthPoint
    {
        [SerializeField] private TextMeshProUGUI healthPontText;

        public void SetHealthPoint(int previousHealth, int newHealth)
        {
            healthPontText.text = $"{newHealth}";
        }
    }
}