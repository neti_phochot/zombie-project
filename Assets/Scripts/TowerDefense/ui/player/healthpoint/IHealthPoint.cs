﻿namespace TowerDefense.ui.player.healthpoint
{
    public interface IHealthPoint
    {
        void SetHealthPoint(int previousHealth, int newHealth);
    }
}