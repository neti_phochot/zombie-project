﻿namespace TowerDefense.ui.wave.counter
{
    public interface IWaveCounter : IBaseUI
    {
        void SetWaveCounter(int currentWave, int maxWave);
    }
}