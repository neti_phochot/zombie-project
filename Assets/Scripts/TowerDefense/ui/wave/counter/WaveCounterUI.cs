﻿using TowerDefense.gameevent;
using UnityEngine;

namespace TowerDefense.ui.wave.counter
{
    public class WaveCounterUI : BaseUI
    {
        private IWaveCounter _waveCounter;

        private void Awake()
        {
            _waveCounter = GetComponent<IWaveCounter>();
            Debug.Assert(_waveCounter != null, "IWaveCounter!=null");
        }

        protected override void RegisterEvents()
        {
            TDEventInstance.GameEvent.WaveChangedEvent += OnWaveChangedEvent;
        }

        private void OnWaveChangedEvent(int wave, int maxWave)
        {
            _waveCounter.SetWaveCounter(wave, maxWave);
        }
    }
}