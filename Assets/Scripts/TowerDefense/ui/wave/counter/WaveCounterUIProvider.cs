﻿using TMPro;
using UnityEngine;

namespace TowerDefense.ui.wave.counter
{
    public class WaveCounterUIProvider : BaseUIProvider, IWaveCounter
    {
        [SerializeField] private TextMeshProUGUI waveCounterText;

        private void Awake()
        {
            waveCounterText.text = "Shoot Dummy Zombie to start!";
        }

        public void SetWaveCounter(int currentWave, int maxWave)
        {
            waveCounterText.text = $"Wave: {currentWave} / {maxWave}";
        }
    }
}