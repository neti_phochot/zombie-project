﻿namespace TowerDefense.ui.wave.timer
{
    public interface IWaveTimer : IBaseUI
    {
        void SetNextWaveCountdown(float time);
    }
}