﻿using TowerDefense.gameevent;
using UnityEngine;

namespace TowerDefense.ui.wave.timer
{
    public class WaveTimerUI : BaseUI
    {
        private IWaveTimer _waveTimer;

        private void Awake()
        {
            _waveTimer = GetComponent<IWaveTimer>();
            Debug.Assert(_waveTimer != null, "IWaveTimer!=null");
        }

        protected override void RegisterEvents()
        {
            TDEventInstance.GameEvent.WaveTimerTickEvent += OnWaveTimerTickEvent;
            TDEventInstance.GameEvent.FinalWaveEvent += OnFinalWaveEvent;
        }

        private void OnWaveTimerTickEvent(float time)
        {
            _waveTimer.SetNextWaveCountdown(time);
        }

        private void OnFinalWaveEvent()
        {
            _waveTimer.Hide();
        }
    }
}