﻿using TMPro;
using UnityEngine;

namespace TowerDefense.ui.wave.timer
{
    public class WaveTimerUIProvider : BaseUIProvider, IWaveTimer
    {
        [SerializeField] private TextMeshProUGUI waveCountdownText;

        private void Awake()
        {
            waveCountdownText.text = string.Empty;
        }

        public void SetNextWaveCountdown(float time)
        {
            waveCountdownText.text = $"Next Wave: {time}s";
        }
    }
}