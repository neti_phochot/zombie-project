﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace TowerDefense.ui.weapon
{
    public class AmmoBarProvider : BaseUIProvider, IAmmoBar
    {
        [SerializeField] private TextMeshProUGUI ammoText;
        [SerializeField] private Image reloadImage;
        [SerializeField] private Image icon;

        public void SetAmmo(int ammo, int maxAmmo)
        {
            ammoText.text = $"{ammo} / - - -";
        }

        public void SetReloadTime(float time, float reloadTime)
        {
            reloadImage.fillAmount = 1f - time / reloadTime;
        }

        public void SetIcon(Sprite sprite)
        {
            icon.sprite = sprite;
        }
    }
}