﻿using UnityEngine;

namespace TowerDefense.ui.weapon
{
    public interface IAmmoBar
    {
        void SetAmmo(int ammo, int maxAmmo);
        void SetReloadTime(float time, float reloadTime);
        void SetIcon(Sprite sprite);
    }
}