﻿using TowerDefense.gameevent;
using TowerDefense.inventory.item.weapon;
using UnityEngine;

namespace TowerDefense.ui.weapon
{
    public class WeaponUI : BaseUI
    {
        private IAmmoBar _ammoBar;
        private IGun _currentGun;

        private void Awake()
        {
            _ammoBar = GetComponent<IAmmoBar>();
            Debug.Assert(_ammoBar != null, "IAmmoBar!=null");
        }

        protected override void RegisterEvents()
        {
            TDEventInstance.GameEvent.WeaponSwitchEvent += WeaponSwitchEvent;
        }

        private void WeaponSwitchEvent(IGun gun)
        {
            _ammoBar.SetIcon(gun.GetIcon());
            gun.ClearEvents();
            gun.AmmoChangedEvent += OnAmmoChangedEvent;
            gun.OutOfAmmoEvent += OnOutOfAmmoEvent;
            gun.ReloadingEvent += OnReloadingEvent;
            _currentGun = gun;
            OnAmmoChangedEvent(gun.GetAmmo(), gun.GetMaxAmmo());
        }

        private void OnOutOfAmmoEvent()
        {
            Debug.Log("NO AMMO!");
        }

        private void OnAmmoChangedEvent(int ammo, int maxAmmo)
        {
            _ammoBar.SetAmmo(ammo, maxAmmo);
        }

        private void OnReloadingEvent(float time)
        {
            _ammoBar.SetReloadTime(time, _currentGun.GetReloadTime());
        }
    }
}