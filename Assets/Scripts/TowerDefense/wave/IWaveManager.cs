﻿using UnityEngine.Events;

namespace TowerDefense.wave
{
    public interface IWaveManager
    {
        event UnityAction FinalWaveEvent;
        event UnityAction<float> WaveTimerTickEvent;
        event UnityAction WaveTimerEndEvent;
        event UnityAction<int, int> WaveChangedEvent;
        IWaveSpawner GetWaveSpawner();
        int GetWave();
        int GetMaxWave();
        bool IsFinalWave();
        Wave GetCurrentWave();
        void NextWave();
        float GetNextWaveTimer();
        IWaveSpawner GetWaveSpawn();
        IWavePath GetWavePath();
    }
}