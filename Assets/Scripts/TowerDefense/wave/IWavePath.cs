﻿using UnityEngine;

namespace TowerDefense.wave
{
    public interface IWavePath
    {
        Vector3[] GetPath(EntranceType entranceType, PathType pathType);
    }
}