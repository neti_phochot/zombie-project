﻿using System.Collections.Generic;
using Core.entity;
using UnityEngine;
using UnityEngine.Events;

namespace TowerDefense.wave
{
    public interface IWaveSpawner
    {
        event UnityAction<Monster> KillEnemyEvent;
        event UnityAction WaveClearedEvent;
        IEnumerable<LivingEntity> GetSpawnedEntities();
        LivingEntity SpawnEnemy(EntityType entityType, Vector3[] paths);
    }
}