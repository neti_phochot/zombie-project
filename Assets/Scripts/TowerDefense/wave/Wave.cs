﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Utils;

namespace TowerDefense.wave
{
    public class Wave
    {
        private readonly WaveSO _waveSo;
        private readonly IWaveSpawner _waveSpawner;
        private readonly IWavePath _wavePath;

        private List<SpawnData> _waveEvent;
        public event UnityAction<float> WaveTimerTickEvent;
        public event UnityAction WaveTimerEndEvent;

        private readonly struct SpawnData
        {
            public readonly WaveSO.WaveEventData WaveData;
            public readonly float EventTime;

            public SpawnData(WaveSO.WaveEventData waveData, float eventTime)
            {
                WaveData = waveData;
                EventTime = eventTime;
            }
        }

        public float WaveTimer => _waveTimer.Countdown;
        private GameTimer _waveTimer;

        public Wave(WaveSO waveSo, IWaveSpawner waveSpawner, IWavePath wavePath)
        {
            _waveSo = waveSo;
            _waveSpawner = waveSpawner;
            _wavePath = wavePath;
        }

        public void Start()
        {
            _waveTimer = new GameTimer();
            _waveTimer.OnCountdownTick += OnCountdownTick;
            _waveTimer.OnTimerEnd += OnTimerEnd;

            _waveTimer.StartTimer(_waveSo.WaveDelay);
            InitSpawnData();
        }

        private void OnCountdownTick(float time)
        {
            WaveTimerTickEvent?.Invoke(time);
        }

        private void OnTimerEnd()
        {
            WaveTimerEndEvent?.Invoke();
        }

        private void InitSpawnData()
        {
            _waveEvent = new List<SpawnData>();

            foreach (var waveEvent in _waveSo.GetWaveEvents)
            {
                var eventTime = waveEvent.eventDelay;
                for (var j = 0; j < waveEvent.count; j++)
                {
                    _waveEvent.Add(new SpawnData(waveEvent, eventTime));
                    eventTime += waveEvent.spawnDelay;
                }
            }
        }

        public void UpdateWaveEvent()
        {
            _waveTimer.UpdateTimer();

            for (var i = 0; i < _waveEvent.Count; i++)
            {
                var data = _waveEvent[i];
                if (_waveTimer.Timer < data.EventTime) continue;
                _waveEvent.Remove(data);
                SpawnEnemy(data.WaveData);
            }
        }

        private void SpawnEnemy(WaveSO.WaveEventData waveEventData)
        {
            var enemyPath = _wavePath.GetPath(waveEventData.entranceType, waveEventData.walkPathType);
            if (enemyPath.Length < 1)
            {
                Debug.LogWarning($"No path found for entrance: {waveEventData.entranceType}");
                return;
            }

            _waveSpawner.SpawnEnemy(waveEventData.entityType, enemyPath);
        }
    }
}