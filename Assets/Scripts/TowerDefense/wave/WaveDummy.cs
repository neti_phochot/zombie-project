﻿using Core.entity;
using Core.world;
using TowerDefense.game;
using TowerDefense.gameevent;
using UnityEngine;

namespace TowerDefense.wave
{
    public class WaveDummy : BaseListener
    {
        [SerializeField] private EntityType dummyType;

        private Entity _dummyZombie;

        private GameManager _gameManager;

        private void Start()
        {
            _gameManager = FindObjectOfType<GameManager>();
            SpawnDummy();
        }

        protected override void RegisterEvents()
        {
            TDEventInstance.GameEvent.WaveChangedEvent += OnWaveChangeEvent;
            TDEventInstance.GameEvent.WaveClearedEvent += OnWaveClearedEvent;
        }

        private void OnWaveClearedEvent()
        {
            if (_gameManager.WaveManager.IsFinalWave()) return;
            SpawnDummy();
        }

        private void OnEntityDamageEvent(Entity entity, float damage)
        {
            RemoveDummy();
            _gameManager.StartGame();
        }

        private void OnWaveChangeEvent(int wave, int maxWave)
        {
            RemoveDummy();
        }

        private void RemoveDummy()
        {
            if (!_dummyZombie) return;
            Destroy(_dummyZombie.gameObject);
        }

        private void SpawnDummy()
        {
            _dummyZombie = WorldInstance.SpawnEntity(dummyType, transform.position);
            _dummyZombie.SetRotation(transform.rotation);
            _dummyZombie.EntityDamageEvent += OnEntityDamageEvent;
        }
    }
}