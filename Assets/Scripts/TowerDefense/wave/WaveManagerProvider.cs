﻿using UnityEngine;
using UnityEngine.Events;

namespace TowerDefense.wave
{
    public class WaveManagerProvider : MonoBehaviour, IWaveManager
    {
        [SerializeField] private WaveSO[] wavesPrefab;

        private IWaveSpawner _waveSpawner;
        private IWavePath _wavePath;

        private Wave[] _waves;
        private int _wave;
        private int _maxWave;
        private int _waveIndex;
        private WaveState _waveState;
        private Wave _currentWave;

        private void Awake()
        {
            _waveSpawner = GetComponent<IWaveSpawner>();
            _wavePath = GetComponent<IWavePath>();
            Debug.Assert(_waveSpawner != null, "IWaveSpawner!=null");
            Debug.Assert(_wavePath != null, "IWavePath!=null");

            _waveState = WaveState.Waiting;
            InitWaves();
        }

        private void FixedUpdate()
        {
            UpdateWave();
        }

        private void UpdateWave()
        {
            if (_waveState != WaveState.Running) return;
            GetCurrentWave().UpdateWaveEvent();
        }

        private void InitWaves()
        {
            _waves = new Wave[wavesPrefab.Length];
            for (var i = 0; i < _waves.Length; i++)
            {
                var waveSo = wavesPrefab[i];
                var wave = new Wave(waveSo, _waveSpawner, _wavePath);
                _waves[i] = wave;
            }

            _maxWave = _waves.Length;
        }

        public event UnityAction FinalWaveEvent;
        public event UnityAction<float> WaveTimerTickEvent;
        public event UnityAction WaveTimerEndEvent;
        public event UnityAction<int, int> WaveChangedEvent;
        public IWaveSpawner GetWaveSpawner() => _waveSpawner;
        public int GetWave() => _wave;
        public int GetMaxWave() => _maxWave;
        public bool IsFinalWave() => _wave >= _maxWave;
        public Wave GetCurrentWave() => _currentWave;

        public void NextWave()
        {
            _wave++;
            _waveState = WaveState.Running;

            _currentWave = _waves[_waveIndex++];
            _currentWave.Start();
            _currentWave.WaveTimerTickEvent += OnWaveTimerTickEvent;
            _currentWave.WaveTimerEndEvent += OnWaveTimerOverEvent;

            if (IsFinalWave()) FinalWaveEvent?.Invoke();
            WaveChangedEvent?.Invoke(GetWave(), GetMaxWave());
        }

        public float GetNextWaveTimer() => GetCurrentWave().WaveTimer;
        public IWaveSpawner GetWaveSpawn() => _waveSpawner;
        public IWavePath GetWavePath() => _wavePath;

        private void OnWaveTimerTickEvent(float time)
        {
            WaveTimerTickEvent?.Invoke(time);
        }

        private void OnWaveTimerOverEvent()
        {
            WaveTimerEndEvent?.Invoke();
        }
    }
}