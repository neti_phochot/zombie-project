﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Utils;

namespace TowerDefense.wave
{
    public class WavePathProvider : MonoBehaviour, IWavePath
    {
        [SerializeField] private WavePath[] wavePaths;

        [Serializable]
        public struct WavePath
        {
            public EntranceType entranceType;
            [SerializeField] private Transform[] leftPath;
            [SerializeField] private Transform[] midPaths;
            [SerializeField] private Transform[] rightPath;

            public Vector3[] GetPath(PathType pathType)
            {
                switch (pathType)
                {
                    case PathType.Right:
                        return rightPath.ToVector3();
                    case PathType.Mid:
                        return midPaths.ToVector3();
                    case PathType.Left:
                        return leftPath.ToVector3();
                }

                return new[] {Vector3.zero};
            }
        }

        private Dictionary<EntranceType, WavePath> _wavePaths;

        private void Awake()
        {
            InitWavePath();
        }

        private void InitWavePath()
        {
            _wavePaths = new Dictionary<EntranceType, WavePath>();
            foreach (var wavePath in wavePaths)
            {
                if (_wavePaths.ContainsKey(wavePath.entranceType)) continue;
                _wavePaths.Add(wavePath.entranceType, wavePath);
            }
        }

        public Vector3[] GetPath(EntranceType entranceType, PathType pathType)
        {
            return !_wavePaths.ContainsKey(entranceType)
                ? new[] {Vector3.zero}
                : _wavePaths[entranceType].GetPath(pathType);
        }
    }
}