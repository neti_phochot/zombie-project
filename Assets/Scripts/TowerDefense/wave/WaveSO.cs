﻿using System;
using System.Collections.Generic;
using Core.entity;
using UnityEngine;

namespace TowerDefense.wave
{
    [CreateAssetMenu(fileName = "New Wave", menuName = "TD/Wave")]
    public class WaveSO : ScriptableObject
    {
        [Serializable]
        public struct WaveEventData
        {
            public float eventDelay;
            public float spawnDelay;
            public EntranceType entranceType;
            public PathType walkPathType;
            public EntityType entityType;
            public int count;
        }

        [SerializeField] private float waveDelay;
        [SerializeField] private WaveEventData[] wavesEvents;
        public float WaveDelay => waveDelay;
        public IEnumerable<WaveEventData> GetWaveEvents => wavesEvents;
    }
}