﻿using System.Collections.Generic;
using Core.entity;
using Core.ui.healthBar;
using Core.world;
using UnityEngine;
using UnityEngine.Events;

namespace TowerDefense.wave
{
    public class WaveSpawnerProvider : MonoBehaviour, IWaveSpawner
    {
        private List<LivingEntity> _spawnedEntities;

        private void Awake()
        {
            _spawnedEntities = new List<LivingEntity>();
        }

        public event UnityAction<Monster> KillEnemyEvent;
        public event UnityAction WaveClearedEvent;

        public IEnumerable<LivingEntity> GetSpawnedEntities()
        {
            return _spawnedEntities;
        }

        public LivingEntity SpawnEnemy(EntityType entityType, Vector3[] paths)
        {
            var startLocation = paths[0];
            var enemy = (Creature) WorldInstance.SpawnEntity(entityType, startLocation);
            var offset = enemy.TargetProvider.GetEyesTransform().localPosition + new Vector3(0, 1);
            HealthBarInstance.SetHealthBar(enemy, HealthBarType.Default, offset);
            enemy.SetDestination(paths);
            enemy.NextDestination();
            _spawnedEntities.Add(enemy);
            enemy.EntityDeathEvent += OnEntityDeathEvent;
            return enemy;
        }

        private void OnEntityDeathEvent(Entity entity)
        {
            var monster = (Monster) entity;
            _spawnedEntities.Remove(monster);

            KillEnemyEvent?.Invoke(monster);
            if (_spawnedEntities.Count < 1)
            {
                WaveClearedEvent?.Invoke();
            }
        }
    }
}