﻿using System.Collections.Generic;
using Singleton;
using TowerDefense.entity.tower;
using TowerDefense.inventory.item.tower;
using UnityEngine;

namespace TowerDefense.world.tower
{
    public class TowerInstance : ResourceSingleton<TowerInstance>
    {
        [SerializeField] private Tower defaultTower;
        [SerializeField] private Tower fastTower;
        [SerializeField] private Tower slowTower;
        [SerializeField] private Tower protectTower;
        [SerializeField] private Tower heavyTower;
        
        private Dictionary<TowerType, Tower> _towerPrefab;

        public override void Awake()
        {
            base.Awake();
            Debug.Assert(defaultTower != null, "defaultTower can't be null!");
            Debug.Assert(fastTower != null, "fastTower can't be null!");
            Debug.Assert(slowTower != null, "slowTower can't be null!");
            Debug.Assert(protectTower != null, "protectTower can't be null!");
            Debug.Assert(heavyTower != null, "heavyTower can't be null!");
            InitEntities();
        }

        private void InitEntities()
        {
            _towerPrefab = new Dictionary<TowerType, Tower>
            {
                {TowerType.Default, defaultTower},
                {TowerType.Fast, fastTower},
                {TowerType.Slow, slowTower},
                {TowerType.Protect, protectTower},
                {TowerType.Heavy, heavyTower},
            };
        }

        public static Tower GetTower(TowerType towerType)
        {
            return Instantiate(Instance._towerPrefab[towerType]);
        }
    }
}