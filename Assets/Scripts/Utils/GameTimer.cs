﻿using System;
using UnityEngine;

namespace Utils
{
    public class GameTimer
    {
        public event Action<float> OnTimerTick;
        public event Action<float> OnCountdownTick;
        public event Action OnTimerEnd;
        public float Timer { get; private set; }
        public float Countdown => _time - Timer + 1;
        public bool IsRunning { get; private set; }

        private float _time;
        private readonly float _updateThreshold;

        private float _timeThreshold;

        public GameTimer(float time = 1f, float updateThreshold = 1f)
        {
            _time = time;
            _updateThreshold = updateThreshold;
        }

        public void UpdateTimer()
        {
            if (!IsRunning) return;
            _timeThreshold -= Time.deltaTime;

            if (_timeThreshold > 0) return;
            _timeThreshold = _updateThreshold;
            Timer += _updateThreshold;

            if (!(Timer > _time))
            {
                OnTimerTick?.Invoke(Timer);
                OnCountdownTick?.Invoke(Countdown);
                return;
            }

            Timer = _time;
            IsRunning = false;
            OnTimerEnd?.Invoke();
        }

        public void SetTimer(float timer)
        {
            _time = timer;
        }

        public void StartTimer()
        {
            Timer = 0f;
            IsRunning = true;
        }

        public void StartTimer(float time)
        {
            StartTimer();
            SetTimer(time);
        }
    }
}