﻿using System.Collections.Generic;
using UnityEngine;

namespace Utils
{
    public static class JExtension
    {
        public static Vector3[] ToVector3(this IReadOnlyList<Transform> locs)
        {
            var locations = new Vector3[locs.Count];
            for (var i = 0; i < locs.Count; i++)
            {
                locations[i] = locs[i].position;
            }

            return locations;
        }
    }
}